---
title: Biografie schreiben
category: 2 aktiv werden
order: 30
---

# Wie schreibe ich eine Biografie?

<img src="{{ "/assets/images/steine/stolpersteine-siegfried-sara-levy-mit-blumen.jpg" | relative_url }}" alt="Gunter Demnig bei der Verlegung" class="w-25 float-right ml-3 mt-3">

Im Mittelpunkt einer Initiative für eine Stolperstein-Verlegung steht die Suche nach der Biografie einer verfolgten Person. Dabei sind die Lebensdaten, die Stationen der Verfolgung oder der Flucht und der letzte freiwillig gewählte Wohnort eines vom Nationalsozialismus verfolgten Menschen essenziell. Dadurch soll ein aktives Gedenken ermöglicht werden: Bürger\*innen recherchieren über die Schicksale von Verfolgten und tragen somit aktiv zum Gedenken bei. Zweck der Forschung ist es daher nicht allein, die notwendigen Daten zu erhalten. Vielmehr soll die Recherche über das Nötigste hinausgehen und das Leben des Menschen in Erinnerung rufen.
Fangen Sie vor Ort an. Versuchen Sie, einen nachbarschaftlichen Kommunikationsprozess zu initiieren. Treten Sie an ehemalige und jetzige Bewohner\*innen, örtliche Vereine, religiöse Gemeinden, Parteien, Schulen, Ausbildungsbetriebe, Hochschulen und Universitäten heran. Suchen Sie Kontakt zu Heimatforscher\*innen und befragen Sie Zeitzeug\*innen oder deren Nachfahren.
Erfragen Sie:
-     Personen: Familienangehörige, Freund\*innen, Klassenkamerad\*innen, Arbeitskolleg*innen
-     Wohnorte: Straße, Wohnung im Haus, weitere Adressen bei Umzügen, Veränderungen im Stadtbild
-     Familie: Geburten, Taufen, Hochzeiten, Jubiläen, Todesfälle, Namensänderungen
-     Ausbildung und Beruf: Schul- und Berufsabschlüsse, berufliche Tätigkeiten, Studium
-     Ereignisse: Veränderungen nach 1933, Reaktion im Ort auf nationalsozialistische Politik

Sie sind für Ihr Rechercheergebnis und den Umgang mit personenbezogenen Daten selbst verantwortlich. Achten Sie daher darauf, dass Ihre Quellennachweise vollständig sind, überprüfen Sie die Daten, die von anderen recherchiert wurden und verweisen Sie gegebenenfalls auf diese Arbeit.

Folgendes Vorgehen ist denkbar:
  -   Ermittlung von Personen und Familien, für die Stolpersteine verlegt werden sollen: Wenn die Namen und der Wohnort bereits bekannt sind, konzentrieren sich die Nachforschungen auf weitere Familienmitglieder oder Hausbewohner\*innen. Es ist aber auch möglich, über die Erforschung der Geschichte eines oder mehrere Häuser während der Zeit des Nationalsozialismus ein Stolperstein-Projekt zu initiieren. Dabei können Sie das Gedenkbuch „Opfer der Verfolgung der Juden unter der nationalsozialistischen Gewaltherrschaft in Deutschland 1933-1945“ verwenden. Die Datenbank enthält Namen und Daten jüdischer Verfolgter. Name oder Wohnort verfolgter Personen können als Ausgangspunkt der Recherche dienen. Im Gedenkbuch finden sich jedoch keine konkreten Adressen. Zugang zu den Akten und den Adressen erhalten Sie über das Brandenburgische Landeshauptarchiv oder über örtliche Archive oder Museen.
-     Ermittlung vor Ort: Suchen Sie zunächst nach Informationen in der unmittelbaren Umgebung des Wohnortes. Fragen Sie nach Familienangehörigen, Zeitzeug\*innen und ehemaligen Bewohner\*innen des Hauses und der Straße.
-     Recherche in öffentlich zugänglichen Publikationen: Wenden Sie sich an Heimatmuseen, Geschichtsvereine sowie die Stadt-, Landes- und Staatsbibliothek.
-     Online-Recherche: Beginnen Sie mit dem Befragen von Suchmaschinen. Schauen Sie in die Online-Kataloge der Bibliotheken, vor allem in die Zeitschriftensammlungen oder auch in die online verfügbaren Materialien. Suchen Sie personenbezogen nach Adressen und Ansprechpartner*innen.
-     Örtliche Archive: Versuchen Sie, vor Ort Dokumente zu finden. Durchforsten Sie beispielsweise Materialien der Stadtverwaltung, der Kirchengemeinde, der Standesämter und von Geschichtsvereinen.

Weitere Anlaufstellen können [die lokalen Stolperstein-Initativen](https://www.stolpersteine-brandenburg.de/de/aktiv_werden/lokale_initiativen.html) sowie [Archive und Gedenkstätten](https://www.stolpersteine-brandenburg.de/de/hintergrund/archive_und_gedenkstaetten.html) sein.
