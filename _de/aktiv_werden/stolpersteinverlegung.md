---
title: Stolpersteinverlegung
category: 2 aktiv werden
order: 10
---

# Wie organisiere ich eine Stolpersteinverlegung?

Mit Stolpersteinen erinnern Bürger\*innen in Gemeinden an Opfer des Nationalsozialismus. Woran sollten Sie achten, wenn Sie eine Stolpersteinverlegung bei Ihnen vor Ort organisieren möchten?

<img src="{{ "/assets/images/steine/gunter-demnig-bei-der-verlegung.jpg" | relative_url }}" alt="Gunter Demnig bei der Verlegung" class="w-25 float-right ml-3 mt-3">

Gunter Demnig hat einen biographischen Bezug zum Land Brandenburg. Er wurde 1948 in Berlin geboren, wuchs dann in Nauen auf und ging auch dort zur Schule. Im Land Brandenburg hat er im Herbst 2003 die ersten Stolpersteine in Neuruppin verlegt. Mittlerweile haben sich vielzählige [Gemeinden diesem Projekt](https://www.stolpersteine-brandenburg.de/de/aktiv_werden/lokale_initiativen.html) angeschlossen. Nutzen Sie die Erfahrungen anderer und suchen Sie für Ihre geplante Verlegung Kontakt zu Initiativen in Ihrer Umgebung.
Am Anfang Ihres Stolpersteinprojektes steht die Forschung. Nutzen Sie hierfür zum Beispiel unsere [Recherche-Tipps](https://www.stolpersteine-brandenburg.de/de/aktiv_werden/recherche.html). Sie sollten dann Ihr Rechercheergebnis zusammenfassen. Dazu können Sie unterschiedliche Publikationsformen wählen, wie beispielsweise:

- Gedenkschrift
- Flyer zu den Stolpersteinen
- PowerPoint-Präsentation
- Ausstellung
- Präsentation auf der Homepage der Gemeinde
- Eigene Website

Ein Belegexemplar der veröffentlichten Biografie oder neuer Quellen sollten Sie an benutzte Archive und Bibliotheken sowie an weitere beteiligte Institutionen übersenden.

## Inschrift

Die Stolpersteine werden einheitlich mit fünf Angaben zur Person beschriftet. Die Daten sollten Sie mindestens zwei Monate vor der Verlegung, möglichst per E-Mail und als Word-Dokument, senden an:

Karin Richert \
E-Mail: <inschriften@stolpersteine.eu>

Postadresse: \
Zülpicher Straße 58e / WE 25 \
50674 Köln

Telefon: 0221 4248077 \
Fax: 0221 25 85194

Bei der Inschrift müssen Sie sich an folgenden Vorgaben orientieren und Großbuchstaben verwenden:

 - Überschrift Als Überschrift wird meist „HIER WOHNTE“ gewählt. Weitere Möglichkeiten sind: HIER LEBTE, HIER LERNTE oder HIER LEHRTE, HIER ARBEITETE, HIER PRAKTIZIERTE, HIER WIRKTE. Es ist auch ein Stein ohne Überschrift möglich.
- Vorname, Name (gegebenenfalls auch Geburtsname)
- Geburtsjahr
- Deportationsjahr und Ort
- Schicksal Als Angaben sind “TOT” oder “ERMORDET” möglich. Für ein unbekanntes Schicksal stehen drei Fragezeichen “???”, statt Selbstmord steht “FLUCHT IN DEN TOD”. Der Begriff “verschollen” wird nicht verwendet, ebenso nicht der Begriff “Tod”, da dieser einen natürlichen Tod nahelegt. Für den Begriff “Emigration” mit der Bedeutung Auswanderung steht “FLUCHT”, Jahreszahl, Zielland.

Jeder Mensch erhält einen eigenen Stein. Ein Anliegen von Gunter Demnig ist es, im Gedenken die Familien wieder zu vereinen. Deshalb werden auch überlebende Familienangehörige einbezogen (Kinder, die in Sicherheit gebracht werden konnten, oder Angehörige, denen die Flucht gelang; KZ-Überlebende und andere). Gedacht wird auch der Menschen, welche die Flucht in den Tod wählten. Darum ist es wichtig, nach der gesamten Familie zu recherchieren, auch um zu verhindern, dass später weitere Steine verlegt werden müssen. Dabei besteht die Gefahr, dass ältere Steine beschädigt werden.

## Genehmigung und Terminabsprache

Die Stolpersteinverlegung wird zunächst mit der Stadt beziehungsweise Gemeinde und dem Tiefbauamt abgesprochen. Hier muss eine „Genehmigung für das Verlegen von Stolpersteinen im öffentlichen Raum“ beantragt werden. Die formale Handhabung ist in den Gemeinden unterschiedlich, aber durch einen Telefonanruf in der Regel zu erfahren.
Erst nachdem Sie die Bestätigung erhalten haben, vereinbaren Sie einen Termin mit Gunter Demnig. Aufgrund zahlreicher Anfragen müssen Sie im Moment eine Wartezeit von einem halben Jahr einplanen. Die Terminabsprache erfolgt schriftlich mit: Anna Warda, E-Mail: [termine@stolpersteine.eu.](mailto:termine@stolpersteine.eu.)

Sollten Sie bei Ihren Nachforschungen auf ausländische Adressen als letzten Wohnort stoßen, schreiben Sie an: [international@stolpersteine.eu](mailto:international@stolpersteine.eu)

Sie sollten auch Eigentümer\*innen sowie die Bewohner\*innen des Hauses, vor dem der Stein liegen wird, informieren. Das ist nicht zwingend, da der Gehweg in der Regel der Stadt gehört. Aber es kann Streit vermeiden helfen und die Aufgeschlossenheit gegenüber diesem Thema fördern.

**Verlegung**

Mit der Verlegung des Steines wird Ihre Initiative auch ein sichtbarer Teil des europäischen Stolpersteinprojektes. In den ersten Jahren hat Gunter Demnig alle Steine selbst verlegt, auch jetzt ist er bemüht, sein Projekt persönlich weiterzuführen. Aufgrund der Vielzahl von Anfragen setzt er zumindest jeden ersten Stolperstein in einer Kommune. Die Arbeiten dauern ungefähr 20 Minuten. Sie werden in eine Gedenkveranstaltung integriert, die individuell gestaltet werden kann:

- Begrüßung oder Worte von Hinterbliebenen oder Familienangehörigen
- Lesung von Zeitzeug\*innen
- Verlesung der Biografie
- Grußwort von kommunalen Vertretern
- Vorstellung der Projektarbeit
- Musikalische Umrahmung
- Aufführung eines Theaterstückes

Wenn an einem Tag mehrere Steine in einer Gemeinde gesetzt werden, sollte jede einzelne Verlegung würdevoll erfolgen. Umrahmend bietet sich eine Auftaktveranstaltung und im Anschluss ein Empfang an. Gunter Demnig hält auf Wunsch auch einen 50-minütigen Vortrag zum Werdegang des Stolpersteinprojektes mit anschließender Diskussionsrunde. Für die PowerPoint-Präsentation müssten Sie einen Beamer und einen Laptop mit genügend Speicherkapazität zur Verfügung stellen.

## Technische Umsetzung

Geben Sie Gunter Demnig eine genaue Beschreibung der Verlegestelle mit Maßangaben, damit er die richtige Ausrüstung, Füllsteine und weiteres Material mitbringen kann. Bewährt hat sich auch die Anwesenheit des Bauamtes bei der Verlegung. Dann kann die Stelle noch einmal exakt festgelegt werden. Fragen Sie auch, ob Sie der zum Tiefbauamt gehörige Bauhof unterstützen kann. Ansonsten ist es ratsam, einen Baubetrieb vor Ort anzusprechen, der bei der Verlegung und der Schuttentsorgung hilft. In einigen Gemeinden bereiten erfahrene Baubetriebe oder der Bauhof die Stellen vor.

Die Steine werden nicht direkt an die Hauswand gelegt, sondern ungefähr in die Mitte des Gehweges, in der Regel direkt vor dem Eingang. Die Steine einer Familie werden mit Fuge nebeneinander oder auch hintereinander gelegt. Die Stolpersteine haben ein Maß von 96 x 96 mm und eine Höhe von 100 mm. Für die Fugen müssen 5 mm eingeplant werden. Der Aushub für das Betonbett darf nur 12 cm tief sein.

Beim Ordnungsamt muss für die Zeit der Verlegung gegebenenfalls eine Sonderparkgenehmigung für Gunter Demnigs Lieferwagen beantragt werden.

## Einladungen

Gestalten Sie Ihr Stolpersteinprojekt öffentlichkeitswirksam. Informieren Sie sowohl im Vorfeld über das Projekt als auch über die konkrete Steinverlegung:

- Angehörige oder Freund\*innen des Opfers
- Projektunterstützer\*innen und -teilnehmer\*innen

Denken Sie bei allen Veranstaltungen an die Forschenden, die Pat\*innen und die Mitarbeiter\*innen in Archiven, Museen, Bibliotheken, …

- Vertreter\*innen der Stadt oder Gemeinde, von Vereinen, Jugendeinrichtungen, Schulen
- Bewohner\*innen, Eigentümer\*innen des Hauses sowie Nachbar\*innen
- Presse (Informieren Sie auch die lokale Wochenzeitungen und Fernsehsender)
- Polizei: Informieren Sie auch die Polizei, ggfls das Ordnungsamt. Beide Behörden können den Straßenverkehr regeln und bei unerwünschten Gästen helfen.

[Die erste Verlegung von Stolpersteinen in Angermünde (4:22)](https://www.youtube.com/watch?v=aR3g5TEq3xg)

## Kosten und Patenschaften

Ein Stolperstein kostet einschließlich der Verlegung 120 Euro. Hinzu kommen eventuell Übernachtungskosten für Gunter Demnig und seinen Fahrer sowie, falls sie einen Vortrag wollen, ein Honorar von 200,- Euro (zuzüglich Mehrwertsteuer). Die Rechnungslegung erfolgt im Nachhinein, aber nur an eine Rechnungsadresse. Viele Stolpersteininitiativen richten Konten ein und rufen zu Spenden auf. Das ist besonders bei mehreren Steinen sinnvoll. Es besteht auch die Möglichkeit [einer Patenschaft oder Patengemeinschaft für Stolpersteine.](https://www.stolpersteine-brandenburg.de/de/aktiv_werden/stolpersteinpatenschaft.html)

## Weiteres Gedenken

In einigen Gemeinden werden regelmäßig Gedenkveranstaltungen durchgeführt, in anderen finden Familientreffen statt. Wenden Sie sich an Stadtführer\*innen, damit diese die Steine in ihren Rundgang integrieren. Außerdem können die örtlichen Schulen das regionalgeschichtliche Thema in ihr Schulcurriculum aufnehmen.
Es bietet sich an, bereits während der Vorbereitung der Verlegung regelmäßige Treffen der Beteiligten und Informationsveranstaltungen durchzuführen. Hier können Sie auch über bereits vorhandene Stolpersteine berichten und der Schicksale gedenken. Indem Sie neue Stolpersteininitiativen vorstellen, können Sie weitere Interessierte und manchmal sogar neue ungeahnte Quellen erschließen. Außerdem ist dies ein geeigneter Rahmen, um für Patenschaften zu werben.
