---
title: Archive und Gedenkstätten
category: 3 hintergrund
order: 50
---

# Archive und Gedenkstätten

Es gibt diverse Anlaufstellen, die bei der Recherche von Biografien verfolgter Menschen zu Rate gezogen werden können. Hier finden Sie eine Übersicht über Archive, Datenbanken und Gedenkstätten in Brandenburg, die bei der Recherche helfen können.

## Wichtige Archive und Datenbanken

### Brandenburgisches Landeshauptarchiv

Für eine Recherche im Landeshauptarchiv können Sie eine formlose schriftliche Anfrage stellen, in der Sie ihre Recherchefrage angeben. Sie erhalten das Rechercheergebnis nach etwa sechs Wochen. Danach können Sie gegebenenfalls einen Termin vereinbaren, bei dem Sie die Akten im Lesesaal einsehen. Falls Sie mit Jugendlichen arbeiten, sollten Sie die Akten zunächst sichten, um sich über den Quellenwert zu informieren. Beachten Sie dabei, dass die Akteneinsicht vorbereitet werden sollte und emotional überfordern kann.

**Kontakt** \
Brandenburgische Landeshauptarchiv \
Am Windmühlenberg 3\
14469 Potsdam\
E-Mail: <poststelle@blha.brandenburg.de> \
[https://blha.brandenburg.de/](https://blha.brandenburg.de/)

Telefon: 0331 5674-0 \
Fax: 0331 5674-212 \
Ansprechpartnerin: Dr. Monika Nakath

### Bundesarchiv

Im Bundesarchiv werden zahlreiche Unterlagen mit personenbezogenen Daten aus der Zeit der NS-Diktatur verwahrt. Für eine Recherche müssen Sie eine formlose schriftliche Anfrage stellen, in der Sie das Thema und den Zweck Ihrer Nachforschung nennen. Recherche und Benutzung erfolgen grundsätzlich vor Ort in den Räumen des Bundesarchivs. Kleinere Nachforschungen können von den Mitarbeiter\*innen des Bundesarchivs übernommen werden. Das Ergebnis wird schriftlich mitgeteilt. Dabei können Gebühren anfallen.

**Kontakt**

Bundesarchiv \
Finckensteinallee 63 \
12205 Berlin\
[https://www.bundesarchiv.de/](https://www.bundesarchiv.de/)

Telefon: 030 187770-0 \
Fax: 030 187770-111 \
E-Mail: <berlin@bundesarchiv.de> \
Archivfachlicher Dienst \
Telefon: 030 187770-420 oder -411

### Internationaler Suchdienst Bad Arolsen

Beim Internationalen Suchdienst können Sie einen schriftlichen Antrag auf wissenschaftliche Forschung stellen. Verwenden Sie dazu das Formular auf der Webseite des Suchdienstes. Es ist möglich, den Forschungsauftrag personenbezogen, aber auch themenbezogen zu stellen. Bei der ortsbezogenen Suche kann nach Orten, Stadtteilen, Firmen oder ähnlichem gesucht werden. Wenn der Suchdienst über Angaben verfügt, erhalten Sie die Rechercheauswertung nach Unterschrift der Benutzererklärung kostenlos. Für das Zusenden von Dokumenten werden Gebühren erhoben, pro Kopie 30-90 Cent, 5,- Euro für eine DVD als Speichermedium.

**Kontakt**

Internationaler Suchdienst Bad Arolsen \
Große Allee 5-9 \
34454 Bad Arolsen\
[https://arolsen-archives.org/](https://arolsen-archives.org/)

Telefon: 05691 629-0 \
Telefax: 05691 629-501

### Yad Vashem

The Holocaust Martyrs‘ and Heroes‘ Remembrance Authority – Yad Vashem ist eine Gedenkstätte in Israel, die an die nationalsozialistische Judenvernichtung erinnert und sie wissenschaftlich dokumentiert. Die Internetpräsenz enthält eine Datenbank mit Namen und biografischen Angaben von 3,6 Millionen Holocaust-Opfern, die Sie online durchsuchen können. Außerdem haben Sie Zugriff auf das Fotoarchiv und verschiedene Listen, wie beispielsweise Transportlisten, Angaben zu Jüdinnen und Juden, die vor der Deportation die Flucht in den Tod wählten, sowie Listen von Überlebenden der Konzentrationslager. Die Bibliothek verfügt über die größte Sammlung von Literatur zum Thema Holocaust, die Sie online einsehen können.

**Kontakt**

Yad Vashem \
P.O.B. 3477 \
Jerusalem 91034 \
Israel\
[https://www.yadvashem.org/](https://www.yadvashem.org/)

Für Recherchen:
International Institute for Holocaust Research \
E-Mail: <research.institute@yadvashem.org.il> \
Telefon: +972 2 6443480 oder +972 2 6443479

> Im Rechercheleitfaden der Berliner Koordinierungsstelle Stolpersteine finden Sie Hinweise auf [Berliner Datenbanken und Archive](https://www.stolpersteine-berlin.de/de/engagement/recherchieren#archive), die Sie für die Recherche verwenden können, da viele Menschen aus Brandenburg abschnittsweise auch in Berlin lebten.

## Gedenkstätten in Brandenburg

### Gedenkstätte Lindenstraße 54/55 für die Opfer politischer Gewalt im 20. Jh.

Die Gedenkstätte Lindenstraße befasst sich in ihrer Dauerausstellung mit der Kontinuität politischer Verfolgung. Die multimediale Ausstellung führt dabei durch verschiedene zeitliche Epochen. Im Mittelpunkt stehen die Verfolgung während der Zeit des Nationalsozialismus, während der sowjetischen Besatzungszeit und zur Zeit der DDR.

Im Jahr 1935 wurde im Haus durch die Nationalsozialisten ein „Erbgesundheitsgericht” eingerichtet, mit der Aufgabe, in justiz-förmiger Weise über Zwangssterilisationen zu entscheiden. Nach der Verlegung des Volksgerichtshofs aus Berlin nach Potsdam diente das Haus in der Lindenstraße ab 1939 als Untersuchungsgefängnis für politische Häftlinge. Eine Vielzahl von Angehörigen verschiedener Widerstandsgruppen wurde hier festgehalten, verurteilt und später andernorts hingerichtet.

1953 wurde der Komplex durch die Staatssicherheit der DDR übernommen und fungierte weiterhin als Untersuchungsgefängnis.

Lindenstraße 54/55 \
14467 Potsdam \
[https://www.gedenkstaette-lindenstrasse.de/](https://www.gedenkstaette-lindenstrasse.de/)

### Gedenkstätte und Museum Sachsenhausen in Oranienburg

Das KZ Sachsenhausen wurde 1936 von den Nationalsozialisten errichtet. Es befindet sich in Oranienburg, nördlich von Berlin. Das Lager diente als Ausbildungsort für KZ-Kommandeure und wurde zur Unterbringung eines SS-Kontingents benutzt. Von 1936 bis 1945 wurden insgesamt ca. 200.000 Menschen nach Sachsenhausen deportiert. Ab August 1941 ermordeten die Nationalsozialisten an diesem Ort zwischen 13.000 und 18.000 russische Kriegsgefangene innerhalb von zehn Wochen.

Angesichts des Vorstoßes der Roten Armee im Frühjahr 1945 wurde das Lager am 21. April 1945 durch die SS geräumt, die übrigen Häftlinge mussten einen Todesmarsch antreten. Polnische und russische Verbände erreichten das Lager am 22. und 23. April und befreiten es.
Im Anschluss des Konzentrationslagers Sachsenhausen wurde der Ort von den sowjetischen Alliierten ab August 1945 zu einem „Sowjetischen Speziallager” umfunktioniert. 1950 wurde das „Sowjetische Speziallager” aufgelöst.

Von 1962 bis zur Wiedervereinigung Deutschlands, war es eine der drei „Nationalen Mahn- und Gedenkstätten der DDR”. Seit 1991 wird die Gedenkstätte von der Stiftung Brandenburgische Gedenkstätten geleitet. 

Straße der Nationen 22 \
16515 Oranienburg \
[https://www.sachsenhausen-sbg.de/](https://www.sachsenhausen-sbg.de/)

### Gedenkstätte für die Euthanasie-Morde in Brandenburg an der Havel

In der Gedenkstätte Brandenburg an der Havel erinnert seit 2012 eine Dauerausstellung an die mehr als 9.000 Menschen, die im Zuge des  „Euthanasie-Programms” an diesem Ort zwischen Januar und August 1940 durch die Nationalsozialisten ermordet wurden. Die „Tötungsanstalt” Brandenburg war einer von sechs Standorten, an denen durch die „Aktion T4” über 70.000 psychisch Kranke und Behinderte im Deutschen Reich ermordet wurden.

Nicolaiplatz 28 \
14770 Brandenburg an der Havel \
[https://www.brandenburg-euthanasie-sbg.de/](https://www.brandenburg-euthanasie-sbg.de/)

### Mahn- und Gedenkstätte Ravensbrück

Das ehemalige Konzentrationslager Ravensbrück war das größte Konzentrationslager für Frauen. Es wurde ab Dezember 1938 durch die SS errichtet und im April 1945 durch die Rote Armee befreit. In diesem Zeitraum wurden dort schätzungsweise 28.000 Häftlinge ermordet. 1959 wurde an diesem Ort die „Nationale Mahn- und Gedenkstätte Ravensbrück“ errichtet, die seit 1993 durch die Stiftung Brandenburgische Gedenkstätten getragen wird. Seit 2013 sind hier die Dauerausstellung „Das Frauen-Konzentrationslager Ravensbrück – Geschichte und Erinnerung“ sowie kleinere vertiefende Dauerausstellungen zu sehen.

Straße der Nationen \
16798 Fürstenberg/Havel \
[https://www.ravensbrueck-sbg.de/](https://www.ravensbrueck-sbg.de/)
