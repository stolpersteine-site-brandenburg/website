---
title: Literatur und Links
category: 3 hintergrund
order: 60
---

# Literatur und Links

**Literatur zu Stolpersteinen**

- Petra T. Fritsche: Stolpersteine – Das Gedächtnis einer Straße. WVB, Berlin 2014.
- Julia Gilowsky/Horst-Alfred Heinrich: Stolpersteine. Eine empirische Annäherung an die alltägliche Rezeption. In: Jahrbuch für Politik und Geschichte. Band 7. 2016–2019. Franz Steiner Verlag, Stuttgart 2020. S. 121–140.
- Hans Hesse: Stolpersteine: Idee, Künstler, Geschichte, Wirkung. Klartext-Verlag, Essen 2017.
- NS-Dokumentationszentrum der Stadt Köln (Hrsg.): Stolpersteine. Gunter Demnig und sein Projekt. Emons, Köln 2007.
- Joachim Rönneper (Hrsg.): Vor meiner Haustür. „Stolpersteine“ von Gunter Demnig. Ein Begleitbuch. Arachne-Verlag, Gelsenkirchen 2010.
- Ulrike Schrader: Die „Stolpersteine“ oder Von der Leichtigkeit des Gedenkens. In: Geschichte im Westen. Zeitschrift für Landes- und Zeitgeschichte. Jahrgang 21. Klartext-Verlag, Essen 2006. S. 173–181.
- Anna Warda: Ein Kunstdenkmal wirft Fragen auf. Die „Stolpersteine“ zwischen Anerkennung und Kritik. In: Zeitgeschichte-Online. 21. März 2017. Online unter : [https://zeitgeschichte-online.de/geschichtskultur/ein-kunstdenkmal-wirft-fragen-auf](https://zeitgeschichte-online.de/geschichtskultur/ein-kunstdenkmal-wirft-fragen-auf).

**Links**

- Stolperstein-Website des Künstlers Gunter Demnig: [http://www.stolpersteine.eu/](http://www.stolpersteine.eu/)
- Koordinierungsstelle Stolpersteine Berlin: [http://www.stolpersteine-berlin.de/](http://www.stolpersteine-berlin.de)
- Suche im Gedenkbuch des Bundesarchivs: [https://www.bundesarchiv.de/gedenkbuch/](https://www.bundesarchiv.de/gedenkbuch/)
- Zentrale Datenbank der Namen der Holocaustopfer: [https://yvng.yadvashem.org/index.html?language=de](https://yvng.yadvashem.org/index.html?language=de)
