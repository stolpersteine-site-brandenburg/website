---
title: Opfer des Nationalsozialismus
category: 3 hintergrund
order: 10
---

# Opfer des Nationalsozialismus


Das nationalsozialistische Regime verfolgte neben Jüdinnen und Juden diverse weitere Menschengruppen, an die ebenfalls mit Stolpersteinen gedacht wird. Jüdinnen und Juden stellen die größte Opfergruppe des Nationalsozialismus dar. Der Antisemitismus war der nationalsozialistischen Ideologie tief eingeschrieben und imaginierte jüdisches Leben als die größte Bedrohung der deutschen Nation.

Die ersten Stolpersteine wurden in Erinnerung an verfolgte Sinti und Roma verlegt. Für das nationalsozialistische Regime galten sie als „minderwertig“, und die Nazis sahen die teilweise praktizierte nomadische Lebensform als gesellschaftliches Problem an. Als „Zigeuner“ wurden in ganz Europa zwischen 90.000 und 150.000 Sinti und Roma während der Zeit des Nationalsozialismus deportiert und ermordet.

Gegner\*innen des Regimes, Mitglieder widerständiger Gruppen oder oppositioneller Parteien wurden unmittelbar nach der Machtübertragung rigoros verfolgt, um die politische Opposition gegen den Nationalsozialismus unmöglich zu machen. In den ersten Jahren wurden vor allem Kommunist\*innen, Sozialdemokrat\*innen und Gewerkschaftsmitglieder verhaftet und in Konzentrationslagern inhaftiert. Wie viele Menschen dem Regime aufgrund politischer Verfolgung zum Opfer gefallen sind, ist bis heute unklar.

Menschen mit geistigen oder körperlichen Behinderungen galten als dem „deutschen Volkskörper“ nicht zugehörig und deshalb als nicht lebenswert. Bis 1939 wurden Hunderttausende Menschen mit Beeinträchtigungen zwangssterilisiert. Später wurden sie im Rahmen der sogenannten Euthanasie-Programme systematisch ermordet.

Als „Asoziale“ verfolgten die Nazis Menschen aus den sozialen Unterschichten, die in der nationalsozialistischen Ideologie ebenfalls als „minderwertig“ galten. Ihnen wurde zugeschrieben, sich nicht in die nationalsozialistische Gemeinschaft einfügen und nützlich einbringen zu können, weshalb sie aus dem „Volkskörper“ ausgeschlossen und verfolgt wurden.

Homosexualität widersprach dem nationalsozialistischen Streben nach einem steten Wachstum des „arischen“ Bevölkerungsteils und dem propagierten Familienbild. Deshalb wurde Homosexualität unter Strafe gestellt und Homosexuelle verfolgt.

Auch Zeug\*innen Jehovas wurden als sogenannte Bibelforscher verfolgt, da sie als Pazifist\*innen den Kriegsdienst und das Zeigen des Hitlergrußes verweigerten.
