---
title: Write a biography
category: 2 Becoming active
order: 30
---
# How to write a biography?

The focus of the initiative for setting a Stolperstein is the search for
the biography of a victim of persecution. The dates of birth and death,
the stages of the persecution or escape and the last voluntarily chosen
place of residence of a person persecuted under the National Socialist
regime are essential. This is intended to allow for active
commemoration: Citizens research the fates of persecuted individuals and
thus actively contribute to commemoration. The purpose of the research
is not only to obtain the necessary data. The research should go beyond
the bare essentials, and while dealing with the individual case, it
should also evoke awareness of the unique life of this one person. You
can start out on your own doorstep. Try to initiate a conversation in
the neighbourhood. Approach former and current residents, local
associations, religious communities, political parties, schools,
training institutions, colleges and universities. Seek contact with
local historians and interview contemporary witnesses or their
descendants. Make enquiries about:

-   People: family members, friends, classmates, work colleagues
-   Places of residence: the street, the flat in the house, other
    addresses in case of moves, changes in the townscape
-   Family: births, baptisms, weddings, anniversaries, deaths, name
    changes
-   Education and career: school and professional qualifications,
    occupations, academic studies
-   Events: changes after 1933, response to National Socialist politics
    in the community

You are responsible for the results of your research and the handling of
personal data. Therefore, make sure that your source references are
complete, check the data researched by others and reference this work if
necessary. You must also make sure that you only publish sources and
original documents for which you have obtained authorisation from the
respective archives or the relatives.

One possible approach could be:

-   Identify individuals and families for whom a Stolperstein is to be
    set: If the names and the place of residence are already known, the
    research focuses on further family members or house residents.
    However, it is also possible to initiate a Stolperstein project by
    researching the history of one or several houses during the National
    Socialist era. You can use the memorial book \"Victims of the
    Persecution of Jews under the National Socialist Tyranny in Germany
    1933 - 1945\". The database contains names and dates of persecuted
    Jews. Their names or places of residence can be used as a starting
    point for the research. The memorial book, however, does not contain
    any specific addresses. Access to the files and the addresses can be
    obtained through the Brandenburg State Archives or through local
    archives or museums.
-   On-site investigation: First look for information in the immediate
    vicinity of the place of residence. Ask for family members,
    contemporary witnesses and former residents of the house and the
    street.
-   Research in available publications: Contact local history museums,
    historical societies and the municipal, state and national
    libraries.
-   Online research: Start by consulting search engines. The snowball
    effect often works well, i.e. they search for key words such as
    location, name and date. Look in the online catalogues of the
    libraries, especially in the journal collections or in the available
    online resources. Search for addresses and contact persons.
-   Local archives: Try to find documents locally. For example, search
    through materials from the city administration, the church
    community, the Jewish community, the registry offices and historical
    societies.
-   Memorials: Many concentration camp memorials have databases that are
    not open to the public in which they have registered prisoner data.
    If you know where the person had been imprisoned, you can contact
    the memorial site directly for information.

For more information on local Stolperstein initiatives as well as
archives and memorial sites check out this website.
