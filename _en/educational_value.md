---
title: Educational value
category: 3 Background
order: 20
---

# Stolpersteine and their educational value

As a decentralised and participatory memorial, Stolpersteine are
suitable for a variety of educational activities. Students, trainees or
extracurricular youth groups can work with the Stolpersteine in
different ways. With the Stolpersteine directory and the interlinked
database this website offers the opportunity to research Stolpersteine
already set in Brandenburg and to look into individual biographies of
persecuted people. This allows the young people to engage with the
Stolpersteine and the life stories of those persecuted in their own
neighbourhood and the affects of National Socialist politics in the
local context. Equally important, the Stolpersteine project encourages
people to gain experience in historical research, which could lead to
further historical projects.

By working with specific personal stories the young people learn about
the mechanisms of exclusion and persecution during National Socialism.
Through tangible examples, they are able to understand how the
radicalisation of discrimination paved the way for systematic mass
murder. In addition, they can be made aware of the diversity of the
victims, because the Stolpersteine commemorate all groups persecuted
during National Socialism: Jews, Sinti and Roma, political and religious
opponents, victims of the "euthanasia" murders, homosexuals,
Jehovah\'s Witnesses and people stigmatised and persecuted as
"anti-social".

A more engrossing educational project is to initiate the creation of a
Stolperstein. You need to plan a development period of about six months
for this activity. The young people will not only learn about historical
research, but also about the organisational tasks surrounding this
specific form of individual and decentralised remembrance. This includes
fundraising, coordination with local institutions, establishing a
dialogue about local history and the organisation and implementation of
a commemorative event.

You can contact Katja Demnig of the Stolperstein Foundation when
undertaking educational programmes. Among other things she will put you
in contact with completed Stolperstein projects and provide you with
information on suitable complementary material.

To contact her, you can write to:   <paedagogik@stolpersteine.eu>

The Berlin Stolpersteine Coordination Office also offers professional
advice and support for educational Stolpersteine projects, but only in
Berlin. You can order supplementary material from the Berlin
Stolperstein Coordination Office for these projects for a fee of eight
euros. It also contains helpful information and suggestions that apply
to projects in Brandenburg.
