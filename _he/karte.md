---
layout: doctype
title: מַפָּה
category: 1 למצוא אבני נגף
order: 5
link_id: map-link
---

<html lang="{{ page.lang | default: site.lang | default: 'en' }}" class="h-100" dir="rtl">

  {%- include head.html -%}

  <body class="h-100">

    <div class="d-flex flex-column h-100">
      {%- include navigation.html -%}

      <div class="w-100 flex-grow-1" id="map"></div>
    </div>

  </body>

</html>
