---
layout: post
title:  "Video ist online: Tagung Gegen das Vergessen 2020"
date:   2021-01-11 12:00:00 +0100
categories: stolpersteine brandenburg
---

Der Videomitschnitt der Veranstaltung „Gegen das Vergessen. Stolpersteine und andere Erinnerungsorte in Brandenburg“ ist online einsehbar.

Am 3. Dezember 2020 kamen bei der Online-Veranstaltung des Aktionsbündnisses Brandenburg Engagierte aus dem ganzen Land zusammen, um über Stolpersteine, über digitales Lernen und Erinnern sowie Handlungsstrategien gegen Antisemitismus zu diskutieren. In diesem Rahmen wurden auch die langjährigen Bemühungen des Aktionsbündnisses vorgestellt, alle Stolpersteine Brandenburgs in einer Datenbank zu erfassen und auf dieser Website zu präsentieren. Abschließend folgte eine Podimsdiskussion über Erinnerungsorte und Gedenken in Brandenburg. Es sprachen Axel Drecoll, Direktor der Stiftung Brandenburgische Gedenkstätten und Leiter der Gedenkstätte und des Museums Sachsenhausen, Katja Demnig, pädagogische Betreuung des Kunstdenkmals Stolpersteine, und Sandra Brenner, Leiterin der Beratungsstelle für lokale Jugendgeschichtsarbeit „Zeitwerk“ im Landesjugendring Brandenburg e.V.

Ein Mitschnitt der Veranstaltung ist nun online gestellt und kann bei [YouTube](https://www.youtube.com/watch?v=ptzV3sKMmGI&feature=youtu.be) angesehen werden.
