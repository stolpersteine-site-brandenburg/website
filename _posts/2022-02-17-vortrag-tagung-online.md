---
layout: post
title:  "Video der Tagung Dezentrales Gedenken 2021 online"
date:   2022-02-17 12:00:00 +0100
categories: stolpersteine brandenburg
---

Der Eröffnungsvortrag von Prof. Dr. Mirim Rürup über politische Herausforderungen der Erinnerungs- und Gedenkarbeit ist nun als Video online. Die Direktorin des Moses Mendelssohn Zentrums für europäisch-jüdische Studien an der Universität Potsdam sprach bei der Tagung Dezentrales Gedenken. Stolpersteine, lokale Geschichtsprojekte und digitales Erinnern am 11. Dezember 2021 in Potsdam. Die Veranstaltung wurde vom Aktionsbündnis Brandenburg gemeinsam mit dem Landesjugendring Brandenburg und in Kooperation mit der Stiftung Brandenburgische Gedenkstätten organisiert.

Der Vortrag kann bei [Youtube](https://www.youtube.com/watch?v=B06L6MZs-g8) angesehen werden.

Mehr Informationen über die Tagung gibt es auch auf der Website des [Aktionsbündnisses Brandenburg](https://aktionsbuendnis-brandenburg.de/tagung-ueber-stolpersteine-in-brandenburg/).
