$(function() {

  if (!document.getElementById('home-slider')) return;

  var stolpersteine,
      stolpersteine_with_inscriptions,
      stolpersteine_with_biographies;

  var getAllStolpersteineWith = function(attribute) {
    return $.grep(stolpersteine, function(stolperstein) {
      return stolperstein.properties[attribute] && stolperstein.properties[attribute].length > 0;
    });
  }

  var getRandomElement = function(array) {
    return array[Math.floor(Math.random() * array.length)];
  }

  var newRandomInscription = function() {
      var stolperstein = getRandomElement(stolpersteine_with_inscriptions),
          $element = $(getRandomElement($('#home-slider .stolperstein:visible')));
      $element.fadeOut(1000, function() {
        $element.html(stolperstein.properties.inscription)
                .data('stolperstein', stolperstein)
                .fadeIn(1000);
      });
      window.setTimeout(newRandomInscription, 10000);
  }

  var showInscriptionsInSlider = function() {
    stolpersteine_with_inscriptions = getAllStolpersteineWith('inscription');
    $('#home-slider .stolperstein').each(function(index, element) {
      var stolperstein = getRandomElement(stolpersteine_with_inscriptions);
      $(element).data('stolperstein', stolperstein)
                .removeClass('d-none')
                .html(stolperstein.properties.inscription);
    });
    window.setTimeout(newRandomInscription, 5000);
  };

  var showBiographies = function() {
    stolpersteine_with_biographies = getAllStolpersteineWith('personImages');
    var stolperstein = getRandomElement(stolpersteine_with_biographies);
    $('.biografie-main').each(function(index, element) {
      $(element).data('stolperstein', stolperstein);
    });
    $('.biografie-text').each(function(index, element) {
      var html = stolperstein.properties.biographyHTML;
      html = html.split("</p><p>")[0];
      html = html.replace(/(<([^>]+)>)/ig,"");
      $(element).html(html);
    });
    $('.biografie-image').each(function(index, element) {
      var url = stolperstein.properties.personImages[0].path;
      $(element).attr('src', window.api_base_url + url);
    });
    $('.biografie-copyright').each(function(index, element) {
      $(element).html(stolperstein.properties.personImages[0].authorshipRemark);
    });
    $('.biografie-slider').addClass('d-md-block');

  };

  var stolpersteineApiCallBack = function(data) {
    stolpersteine = data.features;
    showInscriptionsInSlider();
    showBiographies();

    if (window.location.hash) {
      var stolperstein_id = window.location.hash.replace('#', '');
      var stolperstein = data.features.find(stolperstein => stolperstein.properties.id == stolperstein_id);
      if (stolperstein) {
        window.showStolpersteinModal(stolperstein, true);
      }
    }
  }

  $.getJSON(window.api_url, stolpersteineApiCallBack);

  $(document).on('click', '#home-slider .stolperstein, .biografie-main', function(event) {
    var stolperstein = $(event.target).data('stolperstein');
    if (!stolperstein) {
      stolperstein = $(event.target).parents('.biografie-main').data('stolperstein');
    }
    window.showStolpersteinModal(stolperstein, true);
    event.preventDefault();
  });

});
