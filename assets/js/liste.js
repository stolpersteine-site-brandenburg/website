$(function() {

  if (!document.getElementById('stolpersteine-table')) return;

  var rowRenderFunction = function(data, type, row, meta) {
    if (type == 'display') {
      if (data) {
        return "<a href='#' class='row-link'>" + data + "</a>";
      } else {
        return '';
      }
    } else {
      return data;
    }
  };

  var nameRenderFunction = function(data, type, row, meta) {
    if (type == 'display') {
      if (data) {
        return "<a href='#' class='row-link'>" + data + "</a>";
      } else {
        return '';
      }
    } else if (type == 'sort') {
      return row.properties.FamilyName;
    } else {
      return data;
    }
  };

  var biographyRenderFunction = function(data, type, row, meta) {
    if (type == 'display') {
      if (data) {
        return "<a href='#' class='row-link'>Vorhanden</a>";
      } else {
        return "<a href='#' class='row-link'>Fehlt</a>";
      }
    } else {
      return data;
    }
  };

  var dataTableColumns = [
    {
      title:          "",
      data:           "",
      defaultContent: "<a href='#' class='row-link'><img src='" + mapSetup.iconUrl + "'></a>"
    },
    {
      title:          i18n.t("FullName"),
      data:           "properties.FullName",
      render:         nameRenderFunction,
      defaultContent: "-"
    },
    {
      title:          i18n.t("bornDate"),
      data:           "properties.bornDate",
      render:         rowRenderFunction,
      defaultContent: "-",
      className:      "d-none d-md-table-cell"
    },
    {
      title:          i18n.t("diedDate"),
      data:           "properties.diedDate",
      render:         rowRenderFunction,
      defaultContent: "-",
      className:      "d-none d-md-table-cell"
    },
    {
      title:          i18n.t("street"),
      data:           "properties.street",
      render:         rowRenderFunction,
      defaultContent: "-",
      className:      "d-none d-lg-table-cell"
    },
    {
      title:          i18n.t("postalCode"),
      data:           "properties.postalCode",
      render:         rowRenderFunction,
      defaultContent: "-",
      className:      "d-none d-lg-table-cell"
    },
    {
      title:          i18n.t("city"),
      data:           "properties.city",
      render:         rowRenderFunction,
      defaultContent: "-"
    }
  ];

  if (window.location.search.match(/biography/)) {
    dataTableColumns.push({
      title:          "Biografie",
      data:           "properties.biographyHTML",
      render:         biographyRenderFunction,
      defaultContent: "-"
    })
  }

  var table = $('#stolpersteine-table').on('xhr.dt', function ( e, settings, json, xhr ) {
    if (window.location.hash) {
      var stolperstein_id = window.location.hash.replace('#', '');
      window.stolpersteine = json;
      var stolperstein = json.features.find(stolperstein => stolperstein.properties.id == stolperstein_id);
      if (stolperstein) {
        window.showStolpersteinModal(stolperstein, true);
      }
    }
  }).DataTable({
    order: [1, 'asc'],
    language: jQuery.fn.dataTable.translations[document.documentElement.lang],
    ajax: {
      url:     window.api_url,
      dataSrc: "features"
    },
    columns: dataTableColumns
  } );

  $(document).on('click', '#stolpersteine-table .row-link', function(event) {
    var row = $(event.target).parents('tr').get(0);
    var stolperstein = table.row(row).data();
    window.showStolpersteinModal(stolperstein, true);
    event.preventDefault();
  });

});
