$(function() {
  var modalWrapper = document.getElementById('modal-wrapper'),
      $modalDialog = $(modalWrapper.children[0]),
      modal = new bootstrap.Modal(modalWrapper),
      map = undefined;

  modalWrapper.addEventListener('hidden.bs.modal', function (e) {
    if (map && map.remove) {
      map.off();
      map.remove();
    }
    window.location.hash = '';
  });

  window.showStolpersteinModal = function(stolperstein, showMap) {
    var modalDialogContent = window.renderMustacheTemplate("modal", stolperstein.properties);
    $modalDialog.html(modalDialogContent);

    modal.show();

    if (showMap) {
      var latLng = Array.from(stolperstein.geometry.coordinates).reverse();
      var mapOptions = Object.assign({ zoomControl: false }, window.mapSetup.options);
      map = L.map('popup-map', mapOptions);
      window.mapSetup.tileLayer.addTo(map);

      var marker = L.marker(latLng, { icon: window.mapSetup.icon });
      marker.addTo(map);
      map.setView(latLng, 13);

      map.on({
        click: function() {
          var mapUrl = $('#map-link').attr('href');
          window.location = window.location.origin + mapUrl + "#" + stolperstein.properties.id
        }
      });
    } else {
      $('#popup-map').remove();
    }

    window.location.hash = stolperstein.properties.id;
  };

});
