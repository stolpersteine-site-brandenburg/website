if (!jQuery.fn.dataTable.translations) {
  jQuery.fn.dataTable.translations = {};
}
jQuery.fn.dataTable.translations["de"] = {
    "emptyTable": "Keine Daten in der Tabelle vorhanden",
    "info": "_START_ bis _END_ von _TOTAL_ Einträgen",
    "infoEmpty": "Keine Daten vorhanden",
    "infoFiltered": "(gefiltert von _MAX_ Einträgen)",
    "infoThousands": ".",
    "loadingRecords": "Wird geladen ..",
    "processing": "Bitte warten ..",
    "paginate": {
        "first": "Erste",
        "previous": "Zurück",
        "next": "Nächste",
        "last": "Letzte"
    },
    "aria": {
        "sortAscending": ": aktivieren, um Spalte aufsteigend zu sortieren",
        "sortDescending": ": aktivieren, um Spalte absteigend zu sortieren"
    },
    "select": {
        "rows": {
            "_": "%d Zeilen ausgewählt",
            "1": "1 Zeile ausgewählt"
        },
        "cells": {
            "1": "1 Zelle ausgewählt",
            "_": "%d Zellen ausgewählt"
        },
        "columns": {
            "1": "1 Spalte ausgewählt",
            "_": "%d Spalten ausgewählt"
        }
    },
    "buttons": {
        "print": "Drucken",
        "copy": "Kopieren",
        "copyTitle": "In Zwischenablage kopieren",
        "copySuccess": {
            "_": "%d Zeilen kopiert",
            "1": "1 Zeile kopiert"
        },
        "collection": "Aktionen <span class=\"ui-button-icon-primary ui-icon ui-icon-triangle-1-s\"><\/span>",
        "colvis": "Spaltensichtbarkeit",
        "colvisRestore": "Sichtbarkeit wiederherstellen",
        "copyKeys": "Drücken Sie die Taste <i>ctrl<\/i> oder <i>⌘<\/i> + <i>C<\/i> um die Tabelle<br \/>in den Zwischenspeicher zu kopieren.<br \/><br \/>Um den Vorgang abzubrechen, klicken Sie die Nachricht an oder drücken Sie auf Escape.",
        "csv": "CSV",
        "excel": "Excel",
        "pageLength": {
            "-1": "Alle Zeilen anzeigen",
            "_": "%d Zeilen anzeigen",
            "1": "Zeige 1 Zeile"
        },
        "pdf": "PDF",
        "createState": "Ansicht erstellen",
        "removeAllStates": "Alle Ansichten entfernen",
        "removeState": "Entfernen",
        "renameState": "Umbenennen",
        "savedStates": "Gespeicherte Ansicht",
        "stateRestore": "Ansicht %d",
        "updateState": "Aktualisieren"
    },
    "autoFill": {
        "cancel": "Abbrechen",
        "fill": "Alle Zellen mit <i>%d<i> füllen<\/i><\/i>",
        "fillHorizontal": "Alle horizontalen Zellen füllen",
        "fillVertical": "Alle vertikalen Zellen füllen"
    },
    "decimal": ",",
    "search": "Suche:",
    "searchBuilder": {
        "add": "Bedingung hinzufügen",
        "button": {
            "0": "Such-Baukasten",
            "_": "Such-Baukasten (%d)"
        },
        "condition": "Bedingung",
        "conditions": {
            "date": {
                "after": "Nach",
                "before": "Vor",
                "between": "Zwischen",
                "empty": "Leer",
                "not": "Nicht",
                "notBetween": "Nicht zwischen",
                "notEmpty": "Nicht leer",
                "equals": "Gleich"
            },
            "number": {
                "between": "Zwischen",
                "empty": "Leer",
                "equals": "Entspricht",
                "gt": "Größer als",
                "gte": "Größer als oder gleich",
                "lt": "Kleiner als",
                "lte": "Kleiner als oder gleich",
                "not": "Nicht",
                "notBetween": "Nicht zwischen",
                "notEmpty": "Nicht leer"
            },
            "string": {
                "contains": "Beinhaltet",
                "empty": "Leer",
                "endsWith": "Endet mit",
                "equals": "Entspricht",
                "not": "Nicht",
                "notEmpty": "Nicht leer",
                "startsWith": "Startet mit",
                "notContains": "enthält nicht",
                "notStartsWith": "startet nicht mit",
                "notEndsWith": "endet nicht mit"
            },
            "array": {
                "equals": "ist gleich",
                "empty": "ist leer",
                "contains": "enthält",
                "not": "ist ungleich",
                "notEmpty": "ist nicht leer",
                "without": "aber nicht"
            }
        },
        "data": "Daten",
        "deleteTitle": "Filterregel entfernen",
        "leftTitle": "Äußere Kriterien",
        "logicAnd": "UND",
        "logicOr": "ODER",
        "rightTitle": "Innere Kriterien",
        "title": {
            "0": "Such-Baukasten",
            "_": "Such-Baukasten (%d)"
        },
        "value": "Wert",
        "clearAll": "Alle löschen"
    },
    "searchPanes": {
        "clearMessage": "Leeren",
        "collapse": {
            "0": "Suchmasken",
            "_": "Suchmasken (%d)"
        },
        "countFiltered": "{shown} ({total})",
        "emptyPanes": "Keine Suchmasken",
        "loadMessage": "Lade Suchmasken..",
        "title": "Aktive Filter: %d",
        "showMessage": "zeige Alle",
        "collapseMessage": "Alle einklappen",
        "count": "{total}"
    },
    "thousands": ".",
    "zeroRecords": "Keine passenden Einträge gefunden",
    "lengthMenu": "_MENU_ Zeilen anzeigen",
    "datetime": {
        "previous": "Vorher",
        "next": "Nachher",
        "hours": "Stunden",
        "minutes": "Minuten",
        "seconds": "Sekunden",
        "unknown": "Unbekannt",
        "weekdays": [
            "Sonntag",
            "Montag",
            "Dienstag",
            "Mittwoch",
            "Donnerstag",
            "Freitag",
            "Samstag"
        ],
        "months": [
            "Januar",
            "Februar",
            "März",
            "April",
            "Mai",
            "Juni",
            "Juli",
            "August",
            "September",
            "Oktober",
            "November",
            "Dezember"
        ]
    },
    "editor": {
        "close": "Schließen",
        "create": {
            "button": "Neu",
            "title": "Neuen Eintrag erstellen",
            "submit": "Neu"
        },
        "edit": {
            "button": "Ändern",
            "title": "Eintrag ändern",
            "submit": "ändern"
        },
        "remove": {
            "button": "Löschen",
            "title": "Löschen",
            "submit": "Löschen",
            "confirm": {
                "_": "Sollen %d Zeilen gelöscht werden?",
                "1": "Soll diese Zeile gelöscht werden?"
            }
        },
        "error": {
            "system": "Ein Systemfehler ist aufgetreten"
        },
        "multi": {
            "title": "Mehrere Werte",
            "info": "Die ausgewählten Elemente enthalten mehrere Werte für dieses Feld. Um alle Elemente für dieses Feld zu bearbeiten und auf denselben Wert zu setzen, klicken oder tippen Sie hier, andernfalls behalten diese ihre individuellen Werte bei.",
            "restore": "Änderungen zurücksetzen",
            "noMulti": "Dieses Feld kann nur einzeln bearbeitet werden, nicht als Teil einer Mengen-Änderung."
        }
    },
    "searchPlaceholder": "Suchen...",
    "stateRestore": {
        "creationModal": {
            "button": "Erstellen",
            "columns": {
                "search": "Spalten Suche",
                "visible": "Spalten Sichtbarkeit"
            },
            "name": "Name:",
            "order": "Sortieren",
            "paging": "Seiten",
            "scroller": "Scroll Position",
            "search": "Suche",
            "searchBuilder": "Such-Baukasten",
            "select": "Auswahl",
            "title": "Neue Ansicht erstellen",
            "toggleLabel": "Inkludiert:"
        },
        "duplicateError": "Eine Ansicht mit diesem Namen existiert bereits.",
        "emptyError": "Name darf nicht leer sein.",
        "emptyStates": "Keine gespeicherten Ansichten",
        "removeConfirm": "Bist du dir sicher, dass du %s entfernen möchtest?",
        "removeError": "Entfernen der Ansicht fehlgeschlagen.",
        "removeJoiner": " und ",
        "removeSubmit": "Entfernen",
        "removeTitle": "Ansicht entfernen",
        "renameButton": "Umbenennen",
        "renameLabel": "Neuer Name für %s:",
        "renameTitle": "Ansicht umbenennen"
    }
};
jQuery.fn.dataTable.translations["en"] = {
    "emptyTable": "No data available in table",
    "info": "Showing _START_ to _END_ of _TOTAL_ entries",
    "infoEmpty": "Showing 0 to 0 of 0 entries",
    "infoFiltered": "(filtered from _MAX_ total entries)",
    "infoThousands": ",",
    "lengthMenu": "Show _MENU_ entries",
    "loadingRecords": "Loading...",
    "processing": "Processing...",
    "search": "Search:",
    "zeroRecords": "No matching records found",
    "thousands": ",",
    "paginate": {
        "first": "First",
        "last": "Last",
        "next": "Next",
        "previous": "Previous"
    },
    "aria": {
        "sortAscending": ": activate to sort column ascending",
        "sortDescending": ": activate to sort column descending"
    },
    "autoFill": {
        "cancel": "Cancel",
        "fill": "Fill all cells with <i>%d<\/i>",
        "fillHorizontal": "Fill cells horizontally",
        "fillVertical": "Fill cells vertically"
    },
    "buttons": {
        "collection": "Collection <span class='ui-button-icon-primary ui-icon ui-icon-triangle-1-s'\/>",
        "colvis": "Column Visibility",
        "colvisRestore": "Restore visibility",
        "copy": "Copy",
        "copyKeys": "Press ctrl or u2318 + C to copy the table data to your system clipboard.<br><br>To cancel, click this message or press escape.",
        "copySuccess": {
            "1": "Copied 1 row to clipboard",
            "_": "Copied %d rows to clipboard"
        },
        "copyTitle": "Copy to Clipboard",
        "csv": "CSV",
        "excel": "Excel",
        "pageLength": {
            "-1": "Show all rows",
            "_": "Show %d rows"
        },
        "pdf": "PDF",
        "print": "Print",
        "updateState": "Update",
        "stateRestore": "State %d",
        "savedStates": "Saved States",
        "renameState": "Rename",
        "removeState": "Remove",
        "removeAllStates": "Remove All States",
        "createState": "Create State"
    },
    "searchBuilder": {
        "add": "Add Condition",
        "button": {
            "0": "Search Builder",
            "_": "Search Builder (%d)"
        },
        "clearAll": "Clear All",
        "condition": "Condition",
        "conditions": {
            "date": {
                "after": "After",
                "before": "Before",
                "between": "Between",
                "empty": "Empty",
                "equals": "Equals",
                "not": "Not",
                "notBetween": "Not Between",
                "notEmpty": "Not Empty"
            },
            "number": {
                "between": "Between",
                "empty": "Empty",
                "equals": "Equals",
                "gt": "Greater Than",
                "gte": "Greater Than Equal To",
                "lt": "Less Than",
                "lte": "Less Than Equal To",
                "not": "Not",
                "notBetween": "Not Between",
                "notEmpty": "Not Empty"
            },
            "string": {
                "contains": "Contains",
                "empty": "Empty",
                "endsWith": "Ends With",
                "equals": "Equals",
                "not": "Not",
                "notEmpty": "Not Empty",
                "startsWith": "Starts With",
                "notContains": "Does Not Contain",
                "notStartsWith": "Does Not Start With",
                "notEndsWith": "Does Not End With"
            },
            "array": {
                "without": "Without",
                "notEmpty": "Not Empty",
                "not": "Not",
                "contains": "Contains",
                "empty": "Empty",
                "equals": "Equals"
            }
        },
        "data": "Data",
        "deleteTitle": "Delete filtering rule",
        "leftTitle": "Outdent Criteria",
        "logicAnd": "And",
        "logicOr": "Or",
        "rightTitle": "Indent Criteria",
        "title": {
            "0": "Search Builder",
            "_": "Search Builder (%d)"
        },
        "value": "Value"
    },
    "searchPanes": {
        "clearMessage": "Clear All",
        "collapse": {
            "0": "SearchPanes",
            "_": "SearchPanes (%d)"
        },
        "count": "{total}",
        "countFiltered": "{shown} ({total})",
        "emptyPanes": "No SearchPanes",
        "loadMessage": "Loading SearchPanes",
        "title": "Filters Active - %d",
        "showMessage": "Show All",
        "collapseMessage": "Collapse All"
    },
    "select": {
        "cells": {
            "1": "1 cell selected",
            "_": "%d cells selected"
        },
        "columns": {
            "1": "1 column selected",
            "_": "%d columns selected"
        }
    },
    "datetime": {
        "previous": "Previous",
        "next": "Next",
        "hours": "Hour",
        "minutes": "Minute",
        "seconds": "Second",
        "unknown": "-",
        "amPm": [
            "am",
            "pm"
        ],
        "weekdays": [
            "Sun",
            "Mon",
            "Tue",
            "Wed",
            "Thu",
            "Fri",
            "Sat"
        ],
        "months": [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ]
    },
    "editor": {
        "close": "Close",
        "create": {
            "button": "New",
            "title": "Create new entry",
            "submit": "Create"
        },
        "edit": {
            "button": "Edit",
            "title": "Edit Entry",
            "submit": "Update"
        },
        "remove": {
            "button": "Delete",
            "title": "Delete",
            "submit": "Delete",
            "confirm": {
                "_": "Are you sure you wish to delete %d rows?",
                "1": "Are you sure you wish to delete 1 row?"
            }
        },
        "error": {
            "system": "A system error has occurred (<a target=\"\\\" rel=\"nofollow\" href=\"\\\">More information<\/a>)."
        },
        "multi": {
            "title": "Multiple Values",
            "info": "The selected items contain different values for this input. To edit and set all items for this input to the same value, click or tap here, otherwise they will retain their individual values.",
            "restore": "Undo Changes",
            "noMulti": "This input can be edited individually, but not part of a group. "
        }
    },
    "stateRestore": {
        "renameTitle": "Rename State",
        "renameLabel": "New Name for %s:",
        "renameButton": "Rename",
        "removeTitle": "Remove State",
        "removeSubmit": "Remove",
        "removeJoiner": " and ",
        "removeError": "Failed to remove state.",
        "removeConfirm": "Are you sure you want to remove %s?",
        "emptyStates": "No saved states",
        "emptyError": "Name cannot be empty.",
        "duplicateError": "A state with this name already exists.",
        "creationModal": {
            "toggleLabel": "Includes:",
            "title": "Create New State",
            "select": "Select",
            "searchBuilder": "SearchBuilder",
            "search": "Search",
            "scroller": "Scroll Position",
            "paging": "Paging",
            "order": "Sorting",
            "name": "Name:",
            "columns": {
                "visible": "Column Visibility",
                "search": "Column Search"
            },
            "button": "Create"
        }
    }
};
jQuery.fn.dataTable.translations["he"] = {
    "processing": "מעבד...",
    "lengthMenu": "הצג _MENU_ פריטים",
    "zeroRecords": "לא נמצאו רשומות מתאימות",
    "emptyTable": "לא נמצאו רשומות מתאימות",
    "info": "_START_ עד _END_ מתוך _TOTAL_ רשומות",
    "infoEmpty": "0 עד 0 מתוך 0 רשומות",
    "infoFiltered": "(מסונן מסך _MAX_  רשומות)",
    "search": "חפש:",
    "paginate": {
        "first": "ראשון",
        "previous": "קודם",
        "next": "הבא",
        "last": "אחרון"
    },
    "searchPanes": {
        "clearMessage": "איפוס מסננים",
        "collapse": {
            "0": "מסנני חיפוש",
            "_": "מסנני חיפוש (%d)"
        },
        "count": "ספירה",
        "emptyPanes": "אין מסנני חיפוש",
        "loadMessage": "מסנני חיפוש בטעינה",
        "title": "מסננים פעילים - %d"
    },
    "aria": {
        "sortAscending": "מיון בסדר עולה",
        "sortDescending": "מיון בסדר יורד"
    },
    "autoFill": {
        "cancel": "ביטול",
        "fill": "מלא את כל התאים עם  <i>%d<i><\/i><\/i>",
        "fillHorizontal": "מלא תאים מאוזן",
        "fillVertical": "מלא תאים מאונך"
    },
    "buttons": {
        "collection": "קולקציה",
        "colvis": "נראות עמודות",
        "colvisRestore": "שחזור נראות",
        "copy": "העתק",
        "copySuccess": {
            "1": "רשומה 1 הועתקה",
            "_": "%ds רשומות הועתקו"
        },
        "copyTitle": "העתקת תוכן",
        "csv": "CSV",
        "excel": "Excel",
        "pageLength": {
            "-1": "הצג את כל הרשומות",
            "_": "הצג %d רשומות"
        },
        "pdf": "PDF",
        "print": "הדפס",
        "copyKeys": "לחץ ctrl או u2318 + C על מנת להעתיק את תוכן הטבלה."
    },
    "thousands": ",",
    "datetime": {
        "previous": "הקודם",
        "next": "הבא",
        "hours": "שעה",
        "minutes": "דקה",
        "seconds": "שניה",
        "unknown": "-",
        "amPm": [
            "לפנה\"צ",
            "אחה\"צ"
        ],
        "months": {
            "0": "ינואר",
            "1": "פברואר",
            "10": "נובמבר",
            "11": "דצמבר",
            "2": "מרץ",
            "3": "אפריל",
            "4": "מאי",
            "5": "יוני",
            "6": "יולי",
            "7": "אוגוסט",
            "8": "ספטמבר",
            "9": "אוקטובר"
        },
        "weekdays": [
            "א",
            "ב",
            "ג",
            "ד",
            "ה",
            "ו",
            "ש"
        ]
    },
    "editor": {
        "close": "סגור",
        "create": {
            "button": "חדש",
            "title": "צור רשומה חדשה",
            "submit": "צור"
        },
        "edit": {
            "button": "ערוך",
            "title": "עריכת רשומה",
            "submit": "עדכן"
        },
        "remove": {
            "button": "מחק",
            "title": "מחיקה",
            "submit": "מחק",
            "confirm": {
                "_": "האם אתה בטוח שברצונך למחוק %d רשומות?",
                "1": "האם אתה בטוח שברצונך למחוק רשומה אחת?"
            }
        },
        "error": {
            "system": "אירעה שגיאת מערכת (פרטים נוספים)."
        },
        "multi": {
            "title": "מגוון ערכים שונים",
            "info": "הרשומות שנבחרו מכילים מישע שונה עבור שדה זה. על מנת להגדיר את ששדה זה בכל הרשומות יכיל ערך זהה, לחץ כאן. אחרת הם יישארו עם אותו ערך שהתקבל",
            "restore": "בטל שינוי",
            "noMulti": "שדה זה יכול ליות מוגדר בנפרד אך לא כקבוצה"
        }
    },
    "decimal": "עשרוני",
    "loadingRecords": "טוען...",
    "searchBuilder": {
        "add": "הוסף תנאי",
        "button": {
            "0": "בניית חיפוש",
            "_": "בניית חיפוש (%d)"
        },
        "clearAll": "נקה הכל",
        "condition": "תנאי",
        "conditions": {
            "date": {
                "after": "אחרי",
                "before": "לפני",
                "between": "בין",
                "empty": "ריק",
                "equals": "שווה ל",
                "not": "לא",
                "notBetween": "לא בין",
                "notEmpty": "לא ריק"
            },
            "number": {
                "between": "בין",
                "empty": "ריק",
                "equals": "שווה ל",
                "gt": "גדול מ",
                "gte": "גדול או שווה ל",
                "lt": "קטן מ",
                "lte": "קטן או שווה ל",
                "not": "לא",
                "notBetween": "לא בין",
                "notEmpty": "לא ריק"
            },
            "string": {
                "contains": "מכיל",
                "empty": "ריק",
                "endsWith": "נגמר ב",
                "equals": "שווה ל",
                "not": "לא",
                "notEmpty": "לא ריק",
                "startsWith": "מתחיל ב",
                "notContains": "לא מכיל",
                "notEndsWith": "לא מסתיים ב",
                "notStartsWith": "לא מתחיל ב"
            },
            "array": {
                "equals": "שווה",
                "empty": "ריק",
                "contains": "מכיל",
                "not": "לא",
                "notEmpty": "לא ריק",
                "without": "ללא"
            }
        },
        "data": "תוכן",
        "deleteTitle": "מחיקת חוק סינון",
        "logicAnd": "וגם",
        "logicOr": "או",
        "title": {
            "0": "בניית חיפוש",
            "_": "בניית חיפוש (%d)"
        },
        "value": "ערך"
    },
    "select": {
        "cells": {
            "1": "תא אחד נבחר",
            "_": "%d תאים נבחרו"
        },
        "columns": {
            "1": "עמודה אחת נבחרה",
            "_": "%d עמודות נבחרו"
        },
        "rows": {
            "1": "שורה אחת נבחרה",
            "_": "%d שורות נבחרו"
        }
    }
};
