---
layout: home
---

# Stolpersteine in Brandenburg

<div class="w-50 w-lg-33 w-xxl-25 border p-2 float-left mr-3">
  <img alt="Stolpersteine Velten" src="{{ "/assets/images/steine/Stolpersteine_Velten.jpg
" | relative_url }}" class="mb-1">
  <em class="fw-light font-8">&copy; - Emanuel Neumann</em>
</div>

Beim Gang über die Straßen sind sie kaum zu übersehen: Stolpersteine. Sie erinnern an all jene, die während der Zeit des Nationalsozialismus verfolgt, deportiert und ermordet wurden. Am letzten frei gewählten Wohnort machen sie tagtäglich auf das Schicksal der Verfolgten aufmerksam.

In Brandenburg gibt es 1.300 Stolpersteine. Diese Website stellt auf Grundlage einer Datenbank eine Karte mit Informationen über die in Brandenburg verlegten Stolpersteine und über die verfolgten Menschen zur Verfügung.

[Hier finden Sie die Karte mit allen Stolpersteinen in Brandenburg.](/de/stolpersteine_finden/karte.html)

[Und hinter diesem Link finden Sie eine Liste der Stolpersteine in Brandenburg.](/de/stolpersteine_finden/liste.html)

## Was sind Stolpersteine?

Der Künstler Gunter Demnig hat das Stolperstein-Projekt im Jahr 1992 ins Leben gerufen und damit das weltweit größte dezentrale Mahnmal geschaffen. Inzwischen gibt es über 80.000 Steinen. In manchen Ländern erinnern Adaptionen der Stolpersteine auch an politische Verfolgung und Gewalt, die nicht durch den Nationalsozialismus verursacht wurde. In der Bundesrepublik und in den von Deutschland in den Jahren 1939 bis 1945 besetzten Gebieten in Europa ist die nationalsozialistische Gewaltherrschaft aber weiterhin der zentrale Bezugspunkt. Ein Stolperstein ist ein Betonquader mit einer Messingplatte, der in den Gehweg eingelassen wird. Die Steine werden in Handarbeit vom Bildhauer Michael Friedrichs-Friedländer in Berlin-Pankow hergestellt. Auf der Messingplatte stehen die Namen, die Lebensdaten und die Orte, an die die Menschen deportiert wurden oder an die sie geflohen sind.

In Brandenburg wurden bis 2024 rund 1.300 dieser dezentralen Mahnmale verlegt. Sie mahnen im Stadtbild an die Verbrechen des Nationalsozialismus und halten die Erinnerung an die Verfolgten wach. Auf Initiative des landesweiten Aktionsbündnisses gegen Gewalt, Rechtsextremismus und Rassismus ist diese Website entstanden. Sie begleitet das Stolperstein-Projekt in Brandenburg und zeigt, wo die Steine zu finden sind. Es gibt Orte, in denen dank des lokalen Engagements bereits für all jene, die fliehen mussten oder ermordet wurden, ein Stein verlegt wurde. Dennoch gibt es auch Gemeinden, in denen noch niemand die oft aufwendige Recherche angegangen ist. Hier braucht es Engagierte, um die Geschichte sichtbar zu machen. Von einem Teil der Stolpersteine in Brandenburg fehlen noch Fotos, zum Teil auch weitere Angaben. Helfen Sie bitte mit, diese zu vervollständigen, und [senden Sie uns Fotos und weitere Informationen.](mailto: stolpersteine@aktionsbuendnis-brandenburg.de)
